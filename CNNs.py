import os
import time
import numpy as np


# Importing visualization tools to show results
from PIL import Image
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay,f1_score
import matplotlib.pyplot as plt





def resize_images(images, new_size):
   
    resized_images = np.zeros((images.shape[0], new_size, new_size, images.shape[3]))
    
    # Resize each image to new_size
    for i in range(images.shape[0]):
        resized_images[i] = np.array(Image.fromarray(images[i].astype('uint8')).resize((new_size, new_size)))
    
    return resized_images

def normalize_images(images):
    # Normalize pixel values to the range [0, 1]
    return images.astype('float32') / 255.0

def preprocess_images(images, new_size):
    # Resize images
    resized_images = resize_images(images, new_size)
    
    # Normalize images
    normalized_images = normalize_images(resized_images)
    images_newsize = new_size
    
    return normalized_images,images_newsize


def load_images(folder_path):
    images = []
    labels = []
    class_folders = sorted(os.listdir(folder_path))

    for class_folder in class_folders:
        class_path = os.path.join(folder_path, class_folder)
        if os.path.isdir(class_path):
            image_files = sorted(os.listdir(class_path))
            for image_file in image_files:
                image_path = os.path.join(class_path, image_file)
                image = Image.open(image_path)
                image_array = np.array(image)
                images.append(image_array)
                labels.append(int(class_folder))  # Assuming folder names are class labels

    return np.array(images), np.array(labels)

class ConvLayer:
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0):
        self.filters = np.random.randn(out_channels, kernel_size, kernel_size, in_channels) *  0.01
        self.stride = stride
        self.padding = padding
        self.filters_grad = np.zeros_like(self.filters)  # Initialize gradient accumulation

    def forward(self, inputs):
        batch_size, input_height, input_width, in_channels = inputs.shape
        kernel_size = self.filters.shape[1]
        output_height = (input_height - kernel_size + 2 * self.padding) // self.stride + 1
        output_width = (input_width - kernel_size + 2 * self.padding) // self.stride + 1

        padded_inputs = np.pad(inputs, ((0, 0), (self.padding, self.padding), (self.padding, self.padding), (0, 0)), mode='constant')
        outputs = np.zeros((batch_size, output_height, output_width, self.filters.shape[0]))
    # Perform Convolution operation to input with filter
        for b in range(batch_size):
            for h in range(output_height):
                for w in range(output_width):
                    for c in range(self.filters.shape[0]):
                        h_start = h * self.stride
                        h_end = h_start + kernel_size
                        w_start = w * self.stride
                        w_end = w_start + kernel_size

                        field = padded_inputs[b, h_start:h_end, w_start:w_end, :]
                        outputs[b, h, w, c] = np.sum(field * self.filters[c])

        return outputs

    def backward(self, d_out, inputs):
    
        batch_size, input_height, input_width, in_channels = inputs.shape
        kernel_size = self.filters.shape[1]
        output_height = d_out.shape[1]
        output_width = d_out.shape[2]

        padded_inputs = np.pad(inputs, ((0, 0), (self.padding, self.padding), (self.padding, self.padding), (0, 0)), mode='constant')
        d_filters = np.zeros_like(self.filters)
        d_inputs = np.zeros_like(padded_inputs)

        for b in range(batch_size):
            for h in range(output_height):
                for w in range(output_width):
                    for c in range(self.filters.shape[0]):
                        h_start = h * self.stride
                        h_end = h_start + kernel_size
                        w_start = w * self.stride
                        w_end = w_start + kernel_size

                        field = padded_inputs[b, h_start:h_end, w_start:w_end, :]
                        d_filters[c] += field * d_out[b, h, w, c]
                        d_inputs[b, h_start:h_end, w_start:w_end, :] += self.filters[c] * d_out[b, h, w, c]

        self.filters -= learning_rate * d_filters / batch_size
        self.filters_grad += d_filters
        return d_inputs[:, self.padding:-self.padding, self.padding:-self.padding, :]


class MaxPoolLayer:
    def __init__(self, pool_size=2, stride=2):
        self.pool_size = pool_size
        self.stride = stride

    def forward(self, inputs):
        batch_size, input_height, input_width, num_channels = inputs.shape
        pool_height = int((input_height - self.pool_size) / self.stride) + 1
        pool_width = int((input_width - self.pool_size) / self.stride) + 1
        pooled = np.zeros((batch_size, pool_height, pool_width, num_channels))
    # Perform Pooling operation to input array
        for b in range(batch_size):
            for h in range(pool_height):
                for w in range(pool_width):
                    for c in range(num_channels):
                        h_start = h * self.stride
                        h_end = h_start + self.pool_size
                        w_start = w * self.stride
                        w_end = w_start + self.pool_size

                        field = inputs[b, h_start:h_end, w_start:w_end, c]
                        pooled[b, h, w, c] = np.max(field)

        return pooled

    def backward(self, d_out, inputs):
        batch_size, input_height, input_width, num_channels = inputs.shape
        pool_height = d_out.shape[1]
        pool_width = d_out.shape[2]
        d_inputs = np.zeros_like(inputs)

        for b in range(batch_size):
            for h in range(pool_height):
                for w in range(pool_width):
                    for c in range(num_channels):
                        h_start = h * self.stride
                        h_end = h_start + self.pool_size
                        w_start = w * self.stride
                        w_end = w_start + self.pool_size

                        field = inputs[b, h_start:h_end, w_start:w_end, c]
                        mask = field == np.max(field)
                        d_inputs[b, h_start:h_end, w_start:w_end, c] += mask * d_out[b, h, w, c]

        return d_inputs




class FullyConnectedLayer:
    def __init__(self, input_size, output_size):
        self.weights = np.random.randn(input_size, output_size)  * 0.01
        self.bias = np.zeros((1, output_size))
        self.inputs = None
        self.z = None
        self.activation = None
        self.weights_grad = None
        self.bias_grad = None

    def forward(self, inputs):
        self.inputs = inputs
        self.z = np.dot(inputs, self.weights) + self.bias
        # Applying softmax activation function within the fully connected layer
        exp_scores = np.exp(self.z)
        self.activation = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return self.activation

    def backward(self, d_out):
        num_samples = d_out.shape[0]

        self.weights_grad = np.dot(self.inputs.T, d_out) / num_samples
        self.bias_grad = np.sum(d_out, axis=0, keepdims=True) / num_samples
        d_inputs = np.dot(d_out, self.weights.T)
        return d_inputs

    def update_weights(self, learning_rate):
        self.weights -= learning_rate * self.weights_grad
        self.bias -= learning_rate * self.bias_grad


def categorical_crossentropy(preds, targets):
    epsilon = 1e-10
    preds = np.clip(preds, epsilon, 1 - epsilon)
    batch_size = preds.shape[0]
    
    
    loss = -np.sum(targets * np.log(preds + epsilon)) / batch_size
    
    return loss



def mini_batches(X, y, batch_size):
    num_samples = X.shape[0]
    indices = np.arange(num_samples)
    np.random.shuffle(indices)  # Shuffle indices to create random batches
    for start in range(0, num_samples, batch_size):
        end = min(start + batch_size, num_samples)
        batch_indices = indices[start:end]
        yield X[batch_indices], y[batch_indices]

def predict(X):
    conv_output = conv_layer.forward(X)
    pooled_output = maxpool_layer.forward(conv_output)
    flattened_output = pooled_output.reshape(pooled_output.shape[0], -1)
    scores = softmax_layer.forward(flattened_output)
    
    # Get the index of the highest probability as the predicted class
    predictions = np.argmax(scores, axis=1)
    return predictions

def calculate_accuracy(predictions, targets):
    # Calculate accuracy by comparing predicted labels with true labels
    correct = np.sum(predictions == targets)
    total = len(targets)
    accuracy = correct / total
    return accuracy

train_folder = 'BIRDS\ctraining'
test_folder = 'BIRDS\ctest'

# Load training images and labels
train_images, train_labels = load_images(train_folder)

# Load test images and labels
test_images, test_labels = load_images(test_folder)
num_classes = 10


X_train = train_images 
y_train = train_labels

X_test = train_images 
y_test = train_labels

X_train,image_newsize = preprocess_images(X_train, new_size=64)
X_test, image_newsize= preprocess_images(X_test, new_size=64)

batch_size = 50
num_epochs = 8
learning_rate = 0.1
loss_arr = []
conv_layer = ConvLayer(in_channels=3, out_channels=16, kernel_size=3, stride=2, padding=1)
maxpool_layer = MaxPoolLayer(pool_size=2, stride=2)
softmax_layer = FullyConnectedLayer(image_newsize*image_newsize,10)

start_time = time.time()
for epoch in range(num_epochs):
        epoch_loss = 0.0
        num_batches = 0

        for mini_X, mini_y in mini_batches(X_train, y_train, batch_size):

            # Forward pass
            conv_output = conv_layer.forward(mini_X)
            pooled_output = maxpool_layer.forward(conv_output)
            flattened_output = pooled_output.reshape(pooled_output.shape[0], -1)
            scores = softmax_layer.forward(flattened_output)
            
    
             # One-hot encode the labels for loss calculation
            one_hot_labels = np.zeros_like(scores)
            one_hot_labels[np.arange(len(one_hot_labels)), mini_y] = 1
        
            # Calculate loss
            loss = categorical_crossentropy(scores, one_hot_labels)
            
            
            loss_arr.append(loss)
            
            # Backward pass
            d_scores = (scores - one_hot_labels) / mini_X.shape[0]
            d_flattened = softmax_layer.backward(d_scores)
            d_pooled = d_flattened.reshape(pooled_output.shape)
            d_conv = maxpool_layer.backward(d_pooled, conv_output)
            d_inputs = conv_layer.backward(d_conv, mini_X)
            
            # Print loss every epoch
            print(f"Epoch {epoch+1}/{num_epochs} Batch Number: {num_batches+1} - Loss: {loss}")
            
            # Update weights using gradient descent 
            conv_layer.filters -= learning_rate * conv_layer.filters_grad
            softmax_layer.update_weights(learning_rate)
            # Accumulate loss for this mini-batch
            epoch_loss += loss
            num_batches += 1

        epoch_loss /= num_batches

        print(f"Epoch {epoch+1}/{num_epochs} Overall - Loss: {epoch_loss}")

end_time = time.time()
elapsed_time = end_time - start_time

# Make predictions
test_predictions = predict(X_test)

# Calculate accuracy
accuracy = calculate_accuracy(test_predictions, y_test)
print(f"Test accuracy: {accuracy * 100:.2f}%")
print(f"Elapsed time is {elapsed_time}")

# Plot the loss curve
plt.figure(figsize=(12, 8))
plt.plot(loss_arr)
plt.title('Loss Curve')
plt.xlabel('Iteration')
plt.ylabel('Loss')
plt.show()

# Calculate F1 Score, and print Confusion Matrix
f1score = f1_score(y_test, test_predictions, average='macro')
f1score = round(f1score, 3)
conf_matrix = confusion_matrix(y_test,test_predictions)
disp = ConfusionMatrixDisplay(confusion_matrix = conf_matrix)
disp.plot()
plt.title(f'SVM Confusion Matrix where elapsed time = {elapsed_time}s, F1 Score = {f1score}, Accurracy = {accuracy}')

