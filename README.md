# Bird Image Classification using Convolutional Neural Networks

## Description
Designed a convolutional neural networks (CNNs) algorithm to classify images of bird species,
without using any libraries except Numpy in Python


## Project status
Completed as term project of EE485 Statistical Learning and Data Analytics course
